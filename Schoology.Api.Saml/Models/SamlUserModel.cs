﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoology.Api.Saml.Models
{
    public class SamlUserModel
    {
        public string UId { get; set; }
        public string DisplayName { get; set; }
        public string FirstName { get; set; }
        public string FirstNamePerferred { get; set; }
        public string LastName { get; set; }
        public string SchoolId { get; set; }
        public string SchoolTitle { get; set; }
        public string BuildingId { get; set; }
        public string BuildingTitle { get; set; }
        public string IsAdmin { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public string TimeZoneName { get; set; }
        public string Domain { get; set; }
    }
}
