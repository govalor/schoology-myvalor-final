﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Web.UI.WebControls;
using Schoology.App.MyValor.Models;
using RestSharp;
using RestSharp.Authenticators;
using NLog;
using LoggingLibrary;
using NLog.Targets;
using NLog.Config;
using API_Library_2._0;

namespace Schoology.App.MyValor.Controllers
{
    public class DirectoryController : Controller
    {
        //CM - NLog from logging library
        public static GenerateLogger logGenerator = new GenerateLogger("Active Directory", "https://master-api1.nextworld.net/v2/LoggingConfiguraiton", "jake.vossen@nextworld.net", "gmAHF7eS9!BI$20MzFkk0nW3qz&LdXykwUs!RSBq");
        public static Logger logger = logGenerator.getLogger();

        //CM - using api lib
        //public static SchoologyMyValor apiGen = new SchoologyMyValor();
        public static SchoolDirectory apiGen = new SchoolDirectory();

        private static List<API_Library_2._0.GetParentStudentDirectory_Result> _totalUserList { get; set; }

        private List<API_Library_2._0.GetParentStudentDirectory_Result> LoadStudents(string searchString, List<string> gradefilter)
        {

            // Create the list to hold the users.
            List<API_Library_2._0.GetParentStudentDirectory_Result> filterUserList = new List<API_Library_2._0.GetParentStudentDirectory_Result>();

            // Initialize the Grid.MVC searching, sorting and search string.
            GridMVCModel gridMVCModel = new GridMVCModel();

            ViewBag.gridPage = 1;
            ViewBag.gridFilter = "";
            ViewBag.gridColumn = "";
            ViewBag.gridDir = 0;
            ViewBag.SearchString = "";

            // Proformance Issues
            // We only want to load data when we hit this page.
            // If the post-back for grid paging and filtering, do not reload the data.

            // If no query strings, load the data once and store in static variable
            if (Request.QueryString.Count == 0)
            {

                // Get the Parent's emails using the students EA7RecordsId
                //List<GetParentStudentDirectory_Result> parentsEmailList = db.GetParentStudentDirectory().ToList();
                //_totalUserList = db.GetParentStudentDirectory().ToList(); //Kurt 
                _totalUserList = GetParentsAndStudents(); //Rick Inmann

                // Get the users from Schoology Json File instead of calling the Schoology API directly.
                //_totalUserList = UserService.Instance.GetUsersJsonFile();

                // Set the Grid.MVC grid-page to 1 because if we go to the details page and return it will reload all the data.
                ViewBag.gridPage = 1;

            }
            else
            {
                // Set the view bag variables to be passed to the details page so we can return back to where we started from.
                // Grid Page
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-page"]))
                {
                    ViewBag.gridPage = int.Parse(Request.QueryString["grid-page"]);
                }

                // Grid Filter
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-filter"]))
                {
                    ViewBag.gridFilter = Request.QueryString["grid-filter"];
                }

                // Grid Sort Direction
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-dir"]))
                {
                    ViewBag.gridDir = Request.QueryString["grid-dir"];
                }

                // Grid Sort Column
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-column"]))
                {
                    ViewBag.gridColumn = Request.QueryString["grid-column"];
                }

                // Filter Textbox
                if (!string.IsNullOrWhiteSpace(Request.QueryString["SearchString"]))
                {
                    ViewBag.SearchString = Request.QueryString["SearchString"];
                }

            }

            //List<GetParentStudentDirectory_Result> tmplist = new List<GetParentStudentDirectory_Result>();

            //    }

            // Filter the list by FirstName or LastName.
            if (!string.IsNullOrEmpty(searchString))
            {
                ViewBag.SearchString = searchString;
                
                // Now that we have all the users, check to see if we need to filter them.
                if (!string.IsNullOrEmpty(searchString))
                {

                    if (IsNumeric(searchString))
                    {
                        filterUserList = _totalUserList.Where(s => s.CityStatePostBlock1.Contains(searchString) || s.CityStatePostBlock2.Contains(searchString)).ToList();
                    }
                    else
                    {
                        filterUserList = _totalUserList.Where(s => s.LastName.ToLower().Contains(searchString.ToLower())
                                                                   || s.FirstName.ToLower().Contains(searchString.ToLower())
                                                                   || s.GradeLevel.ToLower().Contains(searchString.ToLower())).ToList();
                    }
                }
            }
            else
            {
                filterUserList = _totalUserList;
            }

            if (gradefilter.Contains("9th") || gradefilter.Contains("10th") || gradefilter.Contains("11th") || gradefilter.Contains("12th"))
            {
                if (gradefilter.Contains("9th") && gradefilter.Count() == 1)
                {
                    filterUserList = filterUserList.Where(t => t.GradeLevel.ToLower() == "9th grade").ToList(); 
                }
                if (gradefilter.Contains("9th") && gradefilter.Contains("10th") && gradefilter.Count() == 2)
                {
                    filterUserList = filterUserList.Where(t => t.GradeLevel.ToLower() == "9th grade" || t.GradeLevel.ToLower() == "10th grade").ToList();
                }
                if (gradefilter.Contains("9th") && gradefilter.Contains("11th") && gradefilter.Count() == 2)
                {
                    filterUserList = filterUserList.Where(t => t.GradeLevel.ToLower() == "9th grade" || t.GradeLevel.ToLower() == "11th grade").ToList();
                }
                if (gradefilter.Contains("9th") && gradefilter.Contains("12th") && gradefilter.Count() == 2)
                {
                    filterUserList = filterUserList.Where(t => t.GradeLevel.ToLower() == "9th grade" || t.GradeLevel.ToLower() == "12th grade").ToList();
                }
                if (gradefilter.Contains("9th") && gradefilter.Contains("10th") && gradefilter.Contains("11th") && gradefilter.Count() == 3)
                {
                    filterUserList = filterUserList.Where(t => t.GradeLevel.ToLower() == "9th grade" || t.GradeLevel.ToLower() == "10th grade" || t.GradeLevel.ToLower() == "11th grade").ToList();
                }
                if (gradefilter.Contains("10th") && gradefilter.Count() == 1)
                {
                    filterUserList = filterUserList.Where(t => t.GradeLevel.ToLower() == "10th grade").ToList();
                }
                if (gradefilter.Contains("10th") && gradefilter.Contains("11th") && gradefilter.Count() == 2)
                {
                    filterUserList = filterUserList.Where(t => t.GradeLevel.ToLower() == "10th grade" || t.GradeLevel.ToLower() == "11th grade").ToList();
                }
                if (gradefilter.Contains("10th") && gradefilter.Contains("12th") && gradefilter.Count() == 2)
                {
                    filterUserList = filterUserList.Where(t => t.GradeLevel.ToLower() == "10th grade" || t.GradeLevel.ToLower() == "12th grade").ToList();
                }
                if (gradefilter.Contains("10th") && gradefilter.Contains("11th") && gradefilter.Contains("12th") && gradefilter.Count() == 3)
                {
                    filterUserList = filterUserList.Where(t => t.GradeLevel.ToLower() == "10th grade" || t.GradeLevel.ToLower() == "11th grade" || t.GradeLevel.ToLower() == "12th grade").ToList();
                }
                if (gradefilter.Contains("11th") && gradefilter.Count() == 1)
                {
                    filterUserList = filterUserList.Where(t => t.GradeLevel.ToLower() == "11th grade").ToList();
                }
                if (gradefilter.Contains("11th") && gradefilter.Contains("12th") && gradefilter.Count() == 2)
                {
                    filterUserList = filterUserList.Where(t => t.GradeLevel.ToLower() == "11th grade" || t.GradeLevel.ToLower() == "12th grade").ToList();
                }
                if (gradefilter.Contains("12th") && gradefilter.Count() == 1)
                {
                    filterUserList = filterUserList.Where(t => t.GradeLevel.ToLower() == "12th grade").ToList();
                }
                //filterUserList = filterUserList.Where(t => t.GradeLevel.Any(s => gradefilter.ToString().Contains(s))).ToList();
            }

            gridMVCModel.GridPage = Convert.ToInt32(ViewBag.gridPage);
            gridMVCModel.GridFilter = ViewBag.gridFilter;
            gridMVCModel.GridColumn = ViewBag.gridColumn;
            gridMVCModel.GridDir = Convert.ToInt32(ViewBag.gridDir);
            gridMVCModel.SearchString = ViewBag.searchString;

            Session["gridMVCModel"] = gridMVCModel;

            //List<Attendance> list = attendanceRoot.attendance.Where(x => x.date == "2015-06-03").ToList();

            return filterUserList;
        }

       /* public static List<API_Library_2._0.GetParentStudentDirectory_Result> GetParentsAndStudents()
        {
            var list = new List<API_Library_2._0.GetParentStudentDirectory_Result>();

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ValorProxy"].ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "[Schoology].[GetParentStudentDirectory2]";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = connection;

                using (var rdr = cmd.ExecuteReader())
                {
                    if (!rdr.HasRows)
                    {
                        logger.Info("Reader has no rows...");
                    }
                    while (rdr.Read())
                    {
                        var row = new API_Library_2._0.GetParentStudentDirectory_Result();
                        row.StudentID = rdr.GetInt32(0);
                        row.DisplayName = rdr.GetString(1);
                        row.LastName = rdr.GetString(2);
                        row.FirstName = rdr.GetString(3);
                        row.MIddleName = rdr.GetString(4);
                        row.GradeLevel = rdr.GetString(5);
                        row.StudentEmail = rdr.GetString(6);
                        row.Siblings = rdr.GetString(7);
                        row.Parents1 = rdr.GetString(8);
                        row.AddressBlock1 = rdr.GetString(9);
                        row.CityStatePostBlock1 = rdr.GetString(10);
                        row.HomePhone1 = rdr.GetString(11);
                        row.Parents2 = rdr.GetString(12);
                        row.AddressBlock2 = rdr.GetString(13);
                        row.CityStatePostBlock2 = rdr.GetString(14);
                        row.HomePhone2 = rdr.GetString(15);
                        list.Add(row);
                    }
                }
            }

            return list;
        }*/
        
        public static List<API_Library_2._0.GetParentStudentDirectory_Result> GetParentsAndStudents()
        {
            List<API_Library_2._0.GetParentStudentDirectory_Result> list = new List<API_Library_2._0.GetParentStudentDirectory_Result>();

            //list = apiGen.getPSDirectory(); //simulation data, no paging

            //list = apiGen.getDirectoryLoop(); //simulation data, paging

            list = apiGen.getParentStudentDirectory(); //bearer authentication and Directory table data

            return list;
        }
        
        public bool IsNumeric(string value)
        {
            return value.All(char.IsNumber);
        }

        //is this ever used??
        [Authorize(Roles = "Admin, School Admin, Front Desk, Parent, Student, Teacher")]
        public ActionResult Index(string searchString, string chbox9, string chbox10, string chbox11, string chbox12)
        {
            var gradefilter = new List<string>();
            if (chbox9 == "true") { gradefilter.Add("9th"); } 
            if (chbox10 == "true") { gradefilter.Add("10th"); } 
            if (chbox11 == "true") { gradefilter.Add("11th"); } 
            if (chbox12 == "true") { gradefilter.Add("12th"); } 

            return View(LoadStudents(searchString, gradefilter));
        }

        [Authorize(Roles = "Admin, School Admin, Front Desk, Parent, Student, Teacher")]
        public ActionResult Details(int id)
        {
            SetGridMVCViewBag();

            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //User user = _totalList.Where(x => x.id == id);
            API_Library_2._0.GetParentStudentDirectory_Result student = _totalUserList.Single(x => x.StudentID == id);

            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);

        }

        private void SetGridMVCViewBag()
        {
            if (Session["gridMVCModel"] != null)
            {
                // Set the Sorting and Filters variables in the view bag to return back to the calling page to return back to where we started from.
                GridMVCModel gridMVCModel = Session["gridMVCModel"] as GridMVCModel;
                ViewBag.gridPage = gridMVCModel.GridPage;
                ViewBag.gridFilter = gridMVCModel.GridFilter;
                ViewBag.gridColumn = gridMVCModel.GridColumn;
                ViewBag.gridDir = gridMVCModel.GridDir;
                ViewBag.SearchString = gridMVCModel.SearchString;
            }
        }

    }
}