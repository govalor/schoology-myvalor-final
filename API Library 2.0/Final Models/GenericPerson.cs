﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class GenericPerson
    {
        public string FirstName { get; internal set; }
        public string NameName { get; internal set; }
        public string LastName { get; internal set; }
        public string PrimaryEmail { get; internal set; }
        public string Gender { get; internal set; }
        public bool Anonymous { get; internal set; }
        public bool TransferStudent { get; internal set; }
        public string EnrollmentStatus { get; internal set; }
        public string GPAElegibilityStatus { get; internal set; }
        public object SocialSecurityNumber { get; internal set; }
        public object VisaNumber { get; internal set; }
        public object PassportNumber { get; internal set; }
        public string PhoneNumber { get; internal set; }
        public String Address { get; internal set; }
    }
}
