﻿using API_Library_2._0.Final_Models;
using API_Library_2._0.Translation_Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class Discovery
    {
        public static HttpBasicAuthenticator auth = new HttpBasicAuthenticator("jake.vossen@nextworld.net", "gmAHF7eS9!BI$20MzFkk0nW3qz&LdXykwUs!RSBq");
        public static TotalFund GetSolicitorData(int sid)
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/JVTotalFund?nwFilter=%7B%22$and%22:%5B%7B%22JVSolicitorID%22:%7B%22$gte%22:" + sid + "%7D%7D,%7B%22JVSolicitorID%22:%7B%22$lte%22:" + sid + "%7D%7D%5D%7D");
            client.Authenticator = auth;

            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            NWTotalFund.RootObject root = JsonConvert.DeserializeObject<NWTotalFund.RootObject>(response.Content);

            return convertToTotalFund(root);

        }

        private static TotalFund convertToTotalFund(NWTotalFund.RootObject root)
        {
            TotalFund t = new TotalFund();
            if (root.Data.records.Count > 0)
            {


                t.funds = (decimal)root.Data.records.ElementAt(0).appData.JVFunds;
                t.guid = Guid.Parse(root.Data.records.ElementAt(0).appData.nwId);
                t.id = root.Data.records.ElementAt(0).appData.JVTotalFundID;
                t.name = root.Data.records.ElementAt(0).appData.JVNickName;
                t.notes = root.Data.records.ElementAt(0).appData.Notes;
                t.solicitorId = root.Data.records.ElementAt(0).appData.JVSolicitorID;

            }
            return t;
        }


        public static List<SolicitorInfo2> GetSolicitorDataForFund(string sid, string fid)
        {
            List<SolicitorInfo2> temp = new List<SolicitorInfo2>();
            var client = new RestClient("https://master-api1.nextworld.net/v2/DonationFundDistribution?nwFilter=%7B%22$and%22:%5B%7B%22Solicitor%22:%7B%22$like%22:%22"+sid+"%22%7D%7D,%7B%22FundID%22:%7B%22$like%22:%22"+fid+"%22%7D%7D%5D%7D&nwPaging=%7B%22limit%22:15,%22offset%22:0%7D");
            client.Authenticator = auth;
            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            NWSolicitorInfo2.RootObject root = JsonConvert.DeserializeObject<NWSolicitorInfo2.RootObject>(response.Content); // This root object is from the DonationFundDistribution. It mostly contains things that are IDs to other things. For this to be usefull, we also have to do quite a few more API Calls. 

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                temp.Add(convertToSolicitorInfo2(root.Data.records.ElementAt(i)));
            }

            return temp;
        }

        private static SolicitorInfo2 convertToSolicitorInfo2(NWSolicitorInfo2.Record record)
        {

            GenericPerson donor = GenGenericPerson.getGenericPerson(record.appData.Donor);
            NWDirectory.RootObject donorDirectory = Directory.getDirectoryRecord(record.appData.Donor);
            NWStudentActivitiesHeader.RootObject activity = StudentActivities.genStudentActivites(record.appData.FundID);
            GenericPerson leader = GenGenericPerson.getGenericPerson(activity.Data.records.ElementAt(0).appData.ActivityLeader);
            NWActivityDefinition.RootObject activityDefinitoin = ActivityDefinition.genActivityDefinition(activity.Data.records.ElementAt(0).appData.Activity);
            SolicitorInfo2 temp = new SolicitorInfo2();

            temp.Address = donor.Address;
            temp.AmountAnonymous = Convert.ToInt32(0); // This needs to be changed. Basically if there is an anon donor they have anonymous in their name after a table lookup. 1 for anon, 0 for normal
            temp.CityStateZip = donorDirectory.Data.records.ElementAt(0).appData.PrimaryAddress.AddressCity + donorDirectory.Data.records.ElementAt(0).appData.PrimaryAddress.AddressState; // This needs to be changed. API Call will return a Direcotry entry and that will have their adress
            temp.Contributor = donor.NameName; // Needs to be changed. API Call will return Donor and get their name from that
            temp.ContributorNotes = record.appData.DonationMemo;
            temp.CreatedOnDate = record.appData.nwCreatedDate;
            temp.DonorAnonymous = Convert.ToInt32(0); // This needs to be changed. Basically if there is an anon donor they have anonymous in their name after a table lookup. 1 for anon, 0 for normal. There might also be the anonomous feild that we can use as well
            temp.Email = donor.PrimaryEmail; // This needs to be changed. The Donor Directory entry will have the email
            temp.ExperienceDescription = activityDefinitoin.Data.records.ElementAt(0).appData.ActivityName; // This needs to change by getting things from the Funds tabed based off the FundID
            temp.FirstName = donor.FirstName; // Will come from directory result
            temp.FundId = record.appData.FundID;
            temp.GiftSolicitorId = record.appData.Solicitor;
            temp.GiftTotal = (decimal)record.appData.DonationAmount.CurrencyValue;
            temp.LastName = donor.LastName; // Will be changed from Direcotry API
            temp.LineItemId = 0; //Not sure what this is going to be used for
            temp.Phone = donor.PhoneNumber; // Needs to be changed from Directory APi 
            temp.TotalFunds = (decimal)record.appData.DonationAmount.CurrencyValue;

            return temp;
        }

        public static List<SolicitorInfo2> GetSolicitorDetailsForFunds(int sid)
        {
            List<SolicitorInfo2> temp = new List<SolicitorInfo2>();
            var client = new RestClient("https://master-api1.nextworld.net/v2/JVGetSolicitorDataForFund?nwFilter=%7B%22$and%22:%5B%7B%22JVSolicitorID%22:%7B%22$gte%22:" + sid + "%7D%7D,%7B%22JVSolicitorID%22:%7B%22$lte%22:" + sid + "%7D%7D%5D%7D&nwSort=JVFundID");
            client.Authenticator = auth;
            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            NWSolicitorInfo2.RootObject root = JsonConvert.DeserializeObject<NWSolicitorInfo2.RootObject>(response.Content);

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                temp.Add(convertToSolicitorInfo2(root.Data.records.ElementAt(i)));
            }

            return temp;
        }
        public static List<SolicitorInfo2> GetLeaderFundData(int lid, string fid)
        {
            List<SolicitorInfo2> temp = new List<SolicitorInfo2>();
            var client = new RestClient("https://master-api1.nextworld.net/v2/JVGetSolicitorDataForFund?nwFilter=%7B%22$and%22:%5B%7B%22JVLeaderID%22:%7B%22$gte%22:"+lid+"%7D%7D,%7B%22JVLeaderID%22:%7B%22$lte%22:"+lid+"%7D%7D,%7B%22JVFundID%22:%7B%22$gte%22:"+fid+"%7D%7D,%7B%22JVFundID%22:%7B%22$lte%22:"+fid+"%7D%7D%5D%7D");
            client.Authenticator = auth;
            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            NWSolicitorInfo2.RootObject root = JsonConvert.DeserializeObject<NWSolicitorInfo2.RootObject>(response.Content);

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                temp.Add(convertToSolicitorInfo2(root.Data.records.ElementAt(i)));
            }

            return temp;
        }



        public static List<LeaderFundDetail> GetLeaderData(int lid, int fid)
        {
            List<LeaderFundDetail> temp = new List<LeaderFundDetail>();

            var client = new RestClient("https://master-api1.nextworld.net/v2/JVLeaderFundDetail?nwFilter=%7B%22$and%22:%5B%7B%22JVLeaderID%22:%7B%22$gte%22:" + lid + "%7D%7D,%7B%22JVLeaderID%22:%7B%22$lte%22:" + lid + "%7D%7D%5D%7D");
            client.Authenticator = auth;
            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            NWLeaderFundDetail.RootObject root = JsonConvert.DeserializeObject<NWLeaderFundDetail.RootObject>(response.Content);

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                temp.Add(convertToLeaderData(root.Data.records.ElementAt(i).appData));
            }

            return temp;
        }

        private static LeaderFundDetail convertToLeaderData(NWLeaderFundDetail.AppData appData)
        {
            LeaderFundDetail temp = new LeaderFundDetail();

            //temp.Address = appData.Address.AddressFull;
            //temp.AmountAnonymous = Convert.ToInt16(appData.JVAmountAnonymous);
            //temp.CityStateZip = appData.JVCityStateZip;
            //temp.Contributor = appData.JVContributor;
            //temp.ContributorNotes = appData.Notes;
            //temp.CreatedOnDate = appData.nwCreatedDate;
            //temp.DonorAnonymous = Convert.ToInt16(appData.JVDonorAnonymous);
            //temp.Email = appData.JVEmail;
            //temp.ExperienceDescription = appData.Description;
            //temp.ExperienceId = appData.JVExperienceID;
            //temp.GiftTotal = (decimal)appData.JVGiftTotal;
            //temp.LeaderId = appData.JVLeaderID;
            //temp.LineItemId = appData.JVLineItemID;
            //temp.Phone = appData.PhoneNumber;
            //temp.SolicitorFirstName = appData.Name.NameName.Substring(0, appData.Name.NameName.IndexOf(" "));
            //temp.SolicitorLastName = appData.Name.NameName.Substring(appData.Name.NameName.IndexOf(" "));
            //temp.TotalFunds = (decimal)appData.JVGiftTotal;
            return temp;
        }

        public static List<SolicitorFunds> GetSolicitorFunds(string nwid) // the nwid is going to be replaced by the actual ID in valor prod. The URL will also have to change to match
        {
            List<SolicitorFunds> temp = new List<SolicitorFunds>();

            var client = new RestClient("https://master-api1.nextworld.net/v2/DonationFundDistribution?nwFilter=%7B%22$and%22:%5B%7B%22Solicitor%22:%7B%22$like%22:%22"+nwid+"%22%7D%7D%5D%7D");
            client.Authenticator = auth;
            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            NWSolicitorFunds.RootObject root = JsonConvert.DeserializeObject<NWSolicitorFunds.RootObject>(response.Content);

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                temp.Add(convertToSolicitorFunds(root.Data.records.ElementAt(i).appData));
            }

            return temp;


        }

        private static SolicitorFunds convertToSolicitorFunds(NWSolicitorFunds.AppData appData)
        {

            NWStudentActivitiesHeader.RootObject activity = StudentActivities.genStudentActivites(appData.FundID);
            NWActivityDefinition.RootObject activityDef = ActivityDefinition.genActivityDefinition(activity.Data.records.ElementAt(0).appData.Activity);
            SolicitorFunds temp = new SolicitorFunds();
            temp.Description = activityDef.Data.records.ElementAt(0).appData.ActivityName;
            temp.FundId = appData.FundID;
            temp.SolicitorId = appData.Solicitor;
            return temp;
        }

        public static List<LeaderFunds> GetLeaderStatus(string nwid)
        {
            List<LeaderFunds> temp = new List<LeaderFunds>();

            NWStudentActivitiesHeader.RootObject activity = StudentActivities.genStudentActivityByLeaderID(nwid);

            for (int i =0; i < activity.Data.records.Count; i++)
            {
                temp.Add(convertToLeaderFunds(activity.Data.records.ElementAt(i), nwid));
            }

            return temp;
        }

        private static LeaderFunds convertToLeaderFunds(NWStudentActivitiesHeader.Record record, string leaderId)
        {
            LeaderFunds temp = new LeaderFunds();

            NWActivityDefinition.RootObject actDef = ActivityDefinition.genActivityDefinition(record.appData.Activity);

            temp.ExperienceDescription = actDef.Data.records.ElementAt(0).appData.ActivityName;
            temp.FundId = record.appData.AssociatedFund;
            temp.LeaderId = leaderId;

            return temp;
        }

        public static List<AdminFundDetail> GetAdminDetail(string start, string end)
        {
            List<AdminFundDetail> temp = new List<AdminFundDetail>();

            var client = new RestClient("https://master-api1.nextworld.net/v2/DonationFundDistribution/");
            client.Authenticator = auth;
            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            NWSolicitorInfo2.RootObject root = JsonConvert.DeserializeObject<NWSolicitorInfo2.RootObject>(response.Content);
            
            for (int i = 50; i < 60; i++)
            {
                temp.Add(convertToAdminFundDetail(root.Data.records.ElementAt(i).appData));
            }
            return temp;
        }

        private static AdminFundDetail convertToAdminFundDetail(NWSolicitorInfo2.AppData appData)
        {
            AdminFundDetail temp = new AdminFundDetail();

            // Getting the infromation from the header
            NWDonationReceiptsTransaction.RootObject donationReceiptTransaction = DonationReceiptTransaction.genDonationReceiptTransaction(appData.nwHeaderId); // this is getting the header detail
            NWDonationReceiptsTransaction.AppData  donationReceptData = donationReceiptTransaction.Data.records.ElementAt(0).appData;

            // fund info
            NWStudentActivitiesHeader.RootObject activity = null;
            NWActivityDefinition.RootObject activityDef = null;
            if (appData.FundID != null)
            {
                activity = StudentActivities.genStudentActivites(appData.FundID);
                if (activity.Data.records.Count > 0)
                {
                    activityDef = ActivityDefinition.genActivityDefinition(activity.Data.records.ElementAt(0).appData.Activity);
                }
            }


            NWDirectory.RootObject donor = Directory.getDirectoryRecord(appData.Donor);
            NWDirectory.AppData donorData = donor.Data.records.ElementAt(0).appData;
            GenericPerson donorPerson = GenGenericPerson.getGenericPerson(appData.Donor);
            NWDonationFundsDetail.RootObject donationDetail = DonationFundDetail.getDonationFundDetail(appData.nwId);
            // getting the solicitor
            NWDirectory.AppData solicitorData = null;
            NWDirectory.RootObject solicitor = Directory.getDirectoryRecord(appData.Solicitor);
            if (solicitor != null)
            {
                solicitorData = solicitor.Data.records.ElementAt(0).appData;

            }
            else
            {
                 solicitorData = null;
            }

            NWDonationFundsDetail.AppData donationData = donationDetail.Data.records.ElementAt(0).appData;

                
            temp.CheckDate = ""+donationReceptData.CheckDate;
            temp.CheckNumber = "" + donationReceptData.CheckNumber;
            if (donorData.PrimaryAddress != null)
            {
                if (donorData.PrimaryAddress.AddressCity != null)
                {
                    temp.CityStateZip = temp.CityStateZip + donorData.PrimaryAddress.AddressCity;
                    temp.City = donorData.PrimaryAddress.AddressCity;
                }

                if (donorData.PrimaryAddress.AddressState != null)
                {
                    temp.CityStateZip = temp.CityStateZip + " " +donorData.PrimaryAddress.AddressState;
                    temp.State = donorData.PrimaryAddress.AddressState;
                }
                if (donorData.PrimaryAddress.AddressPostalCode != null)
                {
                    temp.CityStateZip = temp.CityStateZip + " " + donorData.PrimaryAddress.AddressPostalCode;
                    temp.ZipCode = donorData.PrimaryAddress.AddressPostalCode;
                }
                if (donorData.PrimaryAddress.AddressFull != null)
                {
                    temp.Address = donorData.PrimaryAddress.AddressFull;
                }
            }
            temp.Contributor = donorData.Name.NameName;
            temp.ContributorNotes = appData.DonationMemo;
            temp.CreatedOnDate = "" + appData.nwCreatedDate;
            if (activityDef != null)
            {
                temp.ExperienceDescription = activityDef.Data.records.ElementAt(0).appData.ActivityName;
            }
            temp.FirstName = donorPerson.FirstName; 
            temp.FundId = donationData.FundID; 
            //temp.FundSplitAmount = (decimal)appData.JVFundSplitAmount; //fix me, I don't know what this does
            temp.GiftDate = donationReceptData.PostingDate;
            temp.GiftId = appData.nwId;
            temp.GiftPaymentType = donationReceptData.PaymentMethod;
            //temp.GiftReference = appData.JVGiftReference; // fix me, this should be team + fund
            temp.GiftReferenceDate = donationReceptData.PostingDate;
            if (solicitorData != null && solicitorData.Name != null)
            {
                temp.GiftSolicitorFirstName = solicitorData.Name.NameNamesBeforeKeyNames;
                temp.GiftSolicitorLastName = solicitorData.Name.NameKeyNames; // solicitor from detail view
                    
            }
            temp.GiftSolicitorId = "" + donationData.Solicitor;
            temp.GiftSubType = donationReceptData.GiftInKindAccount;
            temp.GiftType = donationReceptData.GiftClassification; // might be backwards with the one above
            temp.LastName = donorPerson.LastName;
            //temp.LineItemId = appData.JVLineItemID; // No idea what to do with this one
            temp.Name = donorPerson.NameName;
            temp.Phone = donorPerson.PhoneNumber;
            temp.PrimaryEmail = donorPerson.PrimaryEmail;
            temp.SecondaryEmail = donorPerson.PrimaryEmail;
            

            return temp;
        }
    }


}
