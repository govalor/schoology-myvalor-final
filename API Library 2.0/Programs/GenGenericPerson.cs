﻿using API_Library_2._0.Translation_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public static class GenGenericPerson
    {
        public static GenericPerson getGenericPerson(string nwid)
        {
            GenericPerson genericPerson = new GenericPerson();

            NWDirectory.RootObject nwdir = Directory.getDirectoryRecord(nwid);

            NWDirectory.AppData appData = nwdir.Data.records.ElementAt(0).appData;

            genericPerson.NameName = appData.Name.NameName;
            if (genericPerson.NameName != null && genericPerson.NameName != "" && genericPerson.NameName.IndexOf(" ") > 0)
            {
                genericPerson.FirstName = appData.Name.NameName.Substring(0, appData.Name.NameName.IndexOf(" "));
                genericPerson.LastName = appData.Name.NameName.Substring(appData.Name.NameName.IndexOf(" "));
            }
            // genericPerson.PrimaryEmail = appData.Emails.;
            genericPerson.Gender = appData.Gender;
            genericPerson.Anonymous = appData.Anonymous;
            genericPerson.TransferStudent = appData.TransferStudent;
            genericPerson.EnrollmentStatus = appData.EnrollmentStatus;
            genericPerson.GPAElegibilityStatus = appData.GPAEligibilityStatus;
            genericPerson.SocialSecurityNumber = appData.SocialSecurityNumber;
            genericPerson.VisaNumber = appData.VisaNumber;
            genericPerson.PassportNumber = appData.PassportNumber;
            //genericPerson.PhoneNumber = appData.PrimaryPhone;
            
            if (appData.PrimaryAddress != null)
            {
                genericPerson.Address = appData.PrimaryAddress.AddressFull;
            }
            return genericPerson;
        }
    }

}
