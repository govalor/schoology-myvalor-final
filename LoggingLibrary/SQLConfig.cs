﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggingLibrary
{
    class SQLConfig
    {
        public class RootObject
        {
            public string ConnectionString { get; set; }
            public string ConnectionStringCrypto { get; set; }
            public string DBProvider { get; set; }
            public string MinLogLevel { get; set; }
            public string ApplicationName { get; set; }

            public void decrypt()
            {
                if (ConnectionString.Equals(""))
                {
                    ConnectionString = Encrypt.DecryptString(ConnectionStringCrypto, "iIDrAGEuhBhsRxAPUsTr");
                }
            }
        }
    }
}
