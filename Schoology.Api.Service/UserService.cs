﻿using Newtonsoft.Json;
using Schoology.Api.Entities.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Schoology.Api.Service
{
    public class UserService
    {
        private static readonly Lazy<UserService> lazy = new Lazy<UserService>(() => new UserService());

        public ServiceProvider ServiceProvider { get; set; }

        private CacheJsonFile _usersCacheJsonFile;

        public static UserService Instance
        { 
            get 
            { 
                return lazy.Value;
            } 
        }

        public string ConsumerKey
        {
            set { ServiceProvider.ConsumerKey = value; }
        }

        public string ConsumerSecret
        {
            set { ServiceProvider.ConsumerSecret = value; }
        }

        private UserService()
        {
            // Schoology 2-legged Oauth credentials.
            ServiceProvider = ServiceProvider.Instance;
            ServiceProvider.ConsumerKey = ConfigurationManager.AppSettings["consumerKey"];
            ServiceProvider.ConsumerSecret = ConfigurationManager.AppSettings["consumerSecret"];


            _usersCacheJsonFile = new CacheJsonFile(ConfigurationManager.AppSettings["usersJsonSourceFile"], ConfigurationManager.AppSettings["usersJsonDestFile"]);

        }

        public void SetCredentials(string key, string secret)
        {
            ServiceProvider.ConsumerKey = key;
            ServiceProvider.ConsumerSecret = secret;
        }

        public List<User> GetUsersJsonFile()
        {

            // All active courses and sections
            //var json = new WebClient().DownloadString("https://schoology.valorchristian.com/schoologyappattendance/schoologyusers.json");

            // Get active courses and sections from cache file. This is more secure than reading the json file from website from above.
            var json = _usersCacheJsonFile.Json;

            UserRoot root = JsonConvert.DeserializeObject<UserRoot>(json);

            List<User> list = new List<User>();

            list = root.user.ToList();

            // return list  data
            return list;
        }

        public User GetUser(long id)
        {
            List<User> list = new List<User>();

            string api = string.Format("https://api.schoology.com/v1/users/{0}", id);

            string json = ServiceProvider.GetData(api);

            User user = JsonConvert.DeserializeObject<User>(json);

            // If user is not found id should be 0 and lets set the variable to null.
            if (user.id == 0)
                user = null;

            return user;
        }

        public User GetCurrentUser()
        {
            List<User> list = new List<User>();

            string api = string.Format("https://api.schoology.com/v1/users/me");

            string json = ServiceProvider.GetData(api);

            User user = JsonConvert.DeserializeObject<User>(json);

            // If user is not found id should be 0 and lets set the variable to null.
            if (user.id == 0)
                user = null;

            return user;
        }

        public List<Section> GetUserSections(long id)
        {
            // Get the students class schedule
            User user = GetUserJson(id);

            List<Section> userSectionList = user.sections.Distinct().ToList();

            return userSectionList;
        }

        public string GetAppUserInfo()
        {
            List<User> list = new List<User>();

            string api = string.Format("https://api.schoology.com/v1/app-user-info");

            string json = ServiceProvider.GetData(api);

            //User user = JsonConvert.DeserializeObject<User>(json);

            // If user is not found id should be 0 and lets set the variable to null.
            //if (user.id == 0)
            //    user = null;

            return json;
        }

        public List<Role> GetRoles()
        {
            List<User> list = new List<User>();

            string api = string.Format("https://api.schoology.com/v1/roles");

            string json = ServiceProvider.GetData(api);

            RoleRoot roleRoot = JsonConvert.DeserializeObject<RoleRoot>(json);

            return roleRoot.role;
        }

        public List<User> GetUsers(long roleId)
        {
            List<User> list = new List<User>();

            // Schoology API has a max start and limit of 200 to get users.
            // We need to get all of them, so we will loop through them. See code below.

            //string json = ServiceProvider.GetData("https://api.schoology.com/v1/users?role_ids=249463&start=0&limit=200");

            string api = string.Format("https://api.schoology.com/v1/users?role_ids={0}&start=0&limit=200", roleId);
            string json = ServiceProvider.GetData(api);

            UserRoot root = JsonConvert.DeserializeObject<UserRoot>(json);

            list = new List<User>();

            list = list.Concat(root.user).ToList();

            // Loop through the user api and concatenate all the users so we can display, page and filter them.
            while (root.links.next != null)
            {
                int index = root.links.next.IndexOf("start");
                int length = root.links.next.Length;
                int adjust = length - index;

                string nextStartAndLimit = root.links.next.Substring(index, adjust);

                string nextURL = string.Format("https://api.schoology.com/v1/users?role_ids={0}&{1}", roleId, nextStartAndLimit);
                // https://api.schoology.com/v1/users?start=200&limit=200

                json = ServiceProvider.GetData(nextURL);

                root = JsonConvert.DeserializeObject<UserRoot>(json);

                list = list.Concat(root.user).ToList();
            }


            // Alternate way of doing it from about URL.
            // Get only students by role id
            //list = list.Where(s => s.role_id == 249463).ToList();

            return list;
        }


        public List<User> GetUsers()
        {
            List<User> list = new List<User>();

            // Schoology API has a max start and limit of 200 to get users.
            // We need to get all of them, so we will loop through them. See code below.

            // Only get the student users. Student Role Id = 249463
            long roleStudent = Convert.ToInt64(ConfigurationManager.AppSettings["roleStudent"]);
            //string json = ServiceProvider.GetData("https://api.schoology.com/v1/users?role_ids=249463&start=0&limit=200");

            string api = string.Format("https://api.schoology.com/v1/users?role_ids={0}&start=0&limit=200", roleStudent);
            string json = ServiceProvider.GetData(api);

            UserRoot root = JsonConvert.DeserializeObject<UserRoot>(json);

            list = new List<User>();

            list = list.Concat(root.user).ToList();

            // Loop through the user api and concatenate all the users so we can display, page and filter them.
            while (root.links.next != null)
            {
                int index = root.links.next.IndexOf("start");
                int length = root.links.next.Length;
                int adjust = length - index;

                string nextStartAndLimit = root.links.next.Substring(index, adjust);

                string nextURL = string.Format("https://api.schoology.com/v1/users?role_ids={0}&{1}", roleStudent, nextStartAndLimit);
                // https://api.schoology.com/v1/users?start=200&limit=200

                json = ServiceProvider.GetData(nextURL);

                root = JsonConvert.DeserializeObject<UserRoot>(json);

                list = list.Concat(root.user).ToList();
            }


            // Alternate way of doing it from about URL.
            // Get only students by role id
            //list = list.Where(s => s.role_id == 249463).ToList();

            return list;
        }

        public List<Section> GetUsersSectionsApi(long id)
        {
            List<Section> list = new List<Section>();

            // Schoology API has a max start and limit of 200 to get users.
            // We need to get all of them, so we will loop through them. See code below.

            string api = string.Format("https://api.schoology.com/v1/users/{0}/sections", id);

            // Get the user sections
            string json = ServiceProvider.GetData(api);

            SectionRoot root = JsonConvert.DeserializeObject<SectionRoot>(json);

            list = new List<Section>();

            list = root.section.ToList();

            return list;
        }

        public List<Section> GetUsersSectionsFromJsonFile(long id)
        {
            List<Course> list = CourseService.Instance.GetCoursesWithSectionsJsonFile();

            List<Course> studentsCourses = list.Where(x => x.section.Any(s => s.enrollments.Any(e => e.uid == id))).ToList();

            List<Section> studentsSections = new List<Section>();

            foreach (Course course in studentsCourses)
            {
                List<Section> sectionList = course.section.Where(x => x.enrollments.Any(e => e.uid == id)).ToList();
                // A student could be in more than one section i.e. SDSL is a great example.
                if (sectionList.Count > 0)
                {
                    foreach(Section section in sectionList)
                    {
                        studentsSections.Add(section);
                    }

                }

            }

            return studentsSections;
        }
        public List<Section> GetUsersSections(long id)
        {
            // There is a current bug with the API
            // https://api.schoology.com/v1/users/{0}/sections
            // So we use the work around until it get fixed.

            // This the Api method.
            //return GetUsersSectionsApi(id);

            // Workaround method
            return GetUsersSectionsFromJsonFile(id);

        }

        public List<Section> GetUserSectionsJson(long id)
        {
            User user = GetUserJson(id);

            return user.sections;
        }

        public List<Section> GetUserSectionsJson(long id, bool active, bool currentGradingPeriod)
        {
            List<Section> list = GetUserSectionsJson(id);

            // Get only students
            list = list.Where(x => x.admin == 0).ToList();

            // Get only the active sections
            if (active)
            {
                list = list.Where(s => s.active == 1).ToList();
            }

            // Get only the current grading period
            if (currentGradingPeriod)
            {
                long gradingPeriodId = CourseService.Instance.GetCurrentGradingPeriod(CourseService.Instance.GetGradingPeriods());
                list = list.Where(s => s.grading_periods.Contains(gradingPeriodId)).ToList();
            }
            return list;
        }


        public User GetUserJson(long id)
        {

            List<User> list = GetUsersJsonFile();

            list = list.Where(s => s.uid == id).ToList();

            User user = new User();

            if(list.Count > 0)
            {
                // Found the user
                user = list[0];
            }
            else{
                user = null;
            }


            return user;
        }

        public List<Section> GetUsersSections(long id, bool active, bool currentGradingPeriod)
        {
            List<Section> list = GetUsersSections(id);

            // Get only the active sections
            if(active)
            {
                list = list.Where(s => s.active == 1).ToList();
            }

            // Get only the current grading period
            if (currentGradingPeriod)
            {
                long gradingPeriodId = CourseService.Instance.GetCurrentGradingPeriod(CourseService.Instance.GetGradingPeriods());
                list = list.Where(s => s.grading_periods.Contains(gradingPeriodId)).ToList();
            }
            return list;
        }


        //public string PutUserSectionAttendance(long id, long section, DateTime date, int status, string comment)
        //{
        //    string returnValue = "";
        //    //dynamic collectionWrapper = new
        //    //{
        //    //    attendances = currentCourseList
        //    //};



        //    return returnValue;

        //}

        public string PutUserSectionsAttendanceApi(long id, DateTime startDate, DateTime endDate, int status, string comment)
        {
            string returnValue = "";

            // Get the user sections. Calling the API directly
            List<Section> userSectionList = GetUsersSections(id, true, true);

            // Get the sections for the dates the class is scheduled.
            // 1. Get the days numbers from the dates the users will be gone.
            //List<int> weekDayNumbersList = WeekDayNumbers(startDate, endDate);
            // Convert to string becuase the meeting_days is a list of string numbers
            //List<string> days = weekDayNumbersList.ConvertAll<string>(x => x.ToString());

            // 2. Filter the sections on those days the user will be gone.
            // Two ways to do this.
            // 2.1 Use the section meetings_days field.
            // This one is a tough one. Filter the meetings_days list to see if any one of the days.
            // intersects or is in the days list.
            //list = list.FindAll(s => s.meeting_days.Intersect(days).Any()).ToList();

            var weekDayNumbersList = WeekDayNumberDates(startDate, endDate);

            // Create the User Class Schedule.
            List<UserSectionDaySchedule> userSectionDayScheduleList = new List<UserSectionDaySchedule>();

            foreach (Section section in userSectionList)
            {
                // Clear the list
                userSectionDayScheduleList.Clear();

                // Get the enrollments for the section.
                // Get only select the students and not the teachers that are admins.
                // 1. A teacher can be a admin of a section 
                // 2. A teacher can be a admin of a section and not enrolled as a student.
                // 3. A teacher can be a admin of a section and enrolled as a student. This is a rare exception.
                // So we get the enrollments that are not admin. See parameter flag.

                // Get the user sections. Calling the API directly
                section.enrollments = EnrollmentService.Instance.GetSectionEnrollments(section.id, false, false);

                if (section.enrollments.Count > 0)
                {
                    // Get the enrollment Id from each section to set the attendanced.
                    // Note: The enrollment may not be in the section. See #2 from above.
                    section.enrollments = section.enrollments.Where(e => e.uid == id).ToList();

                    // If the count is zero, that means a user is a teacher that is enrolled in the section
                    // and is not enrolled as a student.
                    if (section.enrollments.Count > 0)
                    {
                        foreach (var item in weekDayNumbersList)
                        {
                            // Does the section meet on this day?
                            if (section.meeting_days.Contains(item.Value.ToString()))
                            {
                                UserSectionDaySchedule userSectionDaySchedule = new UserSectionDaySchedule();
                                userSectionDaySchedule.Day = item.Value;
                                userSectionDaySchedule.Date = item.Key;
                                userSectionDaySchedule.sectionId = section.id;

                                userSectionDaySchedule.sectionEnrollmentId = section.enrollments[0].id;

                                userSectionDayScheduleList.Add(userSectionDaySchedule);
                            }
                        }
                    }
                    List<Attendance> attendanceList = new List<Attendance>();

                    foreach (UserSectionDaySchedule userSectionDaySchedule in userSectionDayScheduleList)
                    {

                        Attendance attendance = new Attendance();

                        // Set the attendance status and comment for each section
                        attendance.enrollment_id = userSectionDaySchedule.sectionEnrollmentId;
                        attendance.date = userSectionDaySchedule.Date.ToString("yyyy-MM-dd"); // Date
                        attendance.status = status;
                        attendance.comment = comment;

                        attendanceList.Add(attendance);

                    }
                    // Update the attendance using the Schoology API 
                    AttendanceApiJson attendanceApiJson = new AttendanceApiJson();
                    attendanceApiJson.attendances = new Attendances() { attendance = attendanceList };

                    if (attendanceApiJson.attendances.attendance.Count > 0)
                    {
                        string returnvalue = SectionService.Instance.PutSectionAttendance(section.id, attendanceApiJson);

                        // We need to wait some time in between API calls, otherwise we will get following error:
                        // WebException while reading response - The remote server returned an error: (429) Too Many Requests. - [_endPoint] - https://api.schoology.com/v1/sections/311928497
                        //System.Threading.Thread.Sleep(250); // 1 second. If we still get the error, increase to 2 seconds or 2000
                    }

                }
            }

            // return the HTML success code or error

            return returnValue;

        }

        public string PutUserSectionsAttendanceJson(long id, DateTime startDate, int status, string comment)
        {

            return PutUserSectionsAttendanceJson(id, startDate, startDate, status, comment);

        }

        public string PutUserSectionsAttendanceJson(long id, DateTime startDate, DateTime endDate, int status, string comment)
        {
            string returnValue = "";

            //dynamic collectionWrapper = new
            //{
            //    attendances = currentCourseList
            //};
           
            /*******************************************************/
            /*               PROFORMANCE ENHANCEMENT               */
            /*******************************************************/
            // Get the user sections. Calling the API directly
            //List<Section> userSectionList = GetUsersSections(id, true, true);

            // To improve proformance. Get the json file
            User user = UserService.Instance.GetUserJson(id);
            List<Section> userSectionList = user.sections;

            // Get the sections for the dates the class is scheduled.
            // 1. Get the days numbers from the dates the users will be gone.
            //List<int> weekDayNumbersList = WeekDayNumbers(startDate, endDate);
            // Convert to string becuase the meeting_days is a list of string numbers
            //List<string> days = weekDayNumbersList.ConvertAll<string>(x => x.ToString());

            // 2. Filter the sections on those days the user will be gone.
            // Two ways to do this.
            // 2.1 Use the section meetings_days field.
            // This one is a tough one. Filter the meetings_days list to see if any one of the days.
            // intersects or is in the days list.
            //list = list.FindAll(s => s.meeting_days.Intersect(days).Any()).ToList();

            var weekDayNumbersList = WeekDayNumberDates(startDate, endDate);

            // Create the User Class Schedule.
            List<UserSectionDaySchedule> userSectionDayScheduleList = new List<UserSectionDaySchedule>();

            foreach (Section section in userSectionList)
            {
                // Clear the list
                userSectionDayScheduleList.Clear();

                // Get the enrollments for the section.
                // Get only select the students and not the teachers that are admins.
                // 1. A teacher can be a admin of a section 
                // 2. A teacher can be a admin of a section and not enrolled as a student.
                // 3. A teacher can be a admin of a section and enrolled as a student. This is a rare exception.
                // So we get the enrollments that are not admin. See parameter flag.
                /*******************************************************/
                /*               PROFORMANCE ENHANCEMENT               */
                /*******************************************************/
                // Get the user sections. Calling the API directly
                //section.enrollments = EnrollmentService.Instance.GetSectionEnrollments(section.id, false);

                // To improve proformance. Get the json file
                // We need to check #2 from about
                long enrollmentId = 0;
                if (user.sectionsEnrollmentIds.ContainsKey(section.id))
                {
                    enrollmentId = user.sectionsEnrollmentIds[section.id];
                } 

                if (enrollmentId > 0)
                {

                    // If the count is zero, that means a user is a teacher that is enrolled in the section
                    // and is not enrolled as a student.

                    foreach (var item in weekDayNumbersList)
                    {
                        // Does the section meet on this day?
                        if (section.meeting_days.Contains(item.Value.ToString()))
                        {
                            UserSectionDaySchedule userSectionDaySchedule = new UserSectionDaySchedule();
                            userSectionDaySchedule.Day = item.Value;
                            userSectionDaySchedule.Date = item.Key;
                            userSectionDaySchedule.sectionId = section.id;

                            userSectionDaySchedule.sectionEnrollmentId = enrollmentId;
         
                            userSectionDayScheduleList.Add(userSectionDaySchedule);
                        }
                    }
                    
                    List<Attendance> attendanceList = new List<Attendance>();

                    foreach (UserSectionDaySchedule userSectionDaySchedule in userSectionDayScheduleList)
                    {

                        Attendance attendance = new Attendance();

                        // Set the attendance status and comment for each section
                        attendance.enrollment_id = userSectionDaySchedule.sectionEnrollmentId;
                        attendance.date = userSectionDaySchedule.Date.ToString("yyyy-MM-dd"); // Date
                        attendance.status = status;
                        attendance.comment = comment;

                        attendanceList.Add(attendance);

                    }
                    // Update the attendance using the Schoology API 
                    AttendanceApiJson attendanceApiJson = new AttendanceApiJson();
                    attendanceApiJson.attendances = new Attendances() { attendance = attendanceList };

                    if (attendanceApiJson.attendances.attendance.Count > 0)
                    {
                        string returnvalue = SectionService.Instance.PutSectionAttendance(section.id, attendanceApiJson);

                        // We need to wait some time in between API calls, otherwise we will get following error:
                        // WebException while reading response - The remote server returned an error: (429) Too Many Requests. - [_endPoint] - https://api.schoology.com/v1/sections/311928497
                        System.Threading.Thread.Sleep(250); // 500 = .5 seconds. If we still get the error, increase to 2 seconds or 2000
                    }

                } 
            }
                
            // return the HTML success code or error

            return returnValue;

        }


        public string PutUserSectionsAttendanceByTimeJson(long id, DateTime startDate, int status, string comment, string[] blockColor)
        {
            string returnValue = "";

            //dynamic collectionWrapper = new
            //{
            //    attendances = currentCourseList
            //};

            /*******************************************************/
            /*               PROFORMANCE ENHANCEMENT               */
            /*******************************************************/
            // Get the user sections. Calling the API directly
            //List<Section> userSectionList = GetUsersSections(id, true, true);

            // To improve proformance. Get the json file
            User user = UserService.Instance.GetUserJson(id);
            List<Section> userSectionList = user.sections;

            // Get the sections for the dates the class is scheduled.
            // 1. Get the days numbers from the dates the users will be gone.
            //List<int> weekDayNumbersList = WeekDayNumbers(startDate, endDate);
            // Convert to string becuase the meeting_days is a list of string numbers
            //List<string> days = weekDayNumbersList.ConvertAll<string>(x => x.ToString());

            // 2. Filter the sections on those days the user will be gone.
            // Two ways to do this.
            // 2.1 Use the section meetings_days field.
            // This one is a tough one. Filter the meetings_days list to see if any one of the days.
            // intersects or is in the days list.
            //list = list.FindAll(s => s.meeting_days.Intersect(days).Any()).ToList();

            var weekDayNumbersList = WeekDayNumberDates(startDate, startDate);

            // Create the User Class Schedule.
            List<UserSectionDaySchedule> userSectionDayScheduleList = new List<UserSectionDaySchedule>();

            foreach (Section section in userSectionList)
            {
                // Clear the list
                userSectionDayScheduleList.Clear();

                // Get the enrollments for the section.
                // Get only select the students and not the teachers that are admins.
                // 1. A teacher can be a admin of a section 
                // 2. A teacher can be a admin of a section and not enrolled as a student.
                // 3. A teacher can be a admin of a section and enrolled as a student. This is a rare exception.
                // So we get the enrollments that are not admin. See parameter flag.
                /*******************************************************/
                /*               PROFORMANCE ENHANCEMENT               */
                /*******************************************************/
                // Get the user sections. Calling the API directly
                //section.enrollments = EnrollmentService.Instance.GetSectionEnrollments(section.id, false);

                // To improve proformance. Get the json file
                // We need to check #2 from about
                long enrollmentId = 0;
                if (user.sectionsEnrollmentIds.ContainsKey(section.id))
                {
                    enrollmentId = user.sectionsEnrollmentIds[section.id];
                }

                if (enrollmentId > 0)
                {

                    // If the count is zero, that means a user is a teacher that is enrolled in the section
                    // and is not enrolled as a student.

                    foreach (var item in weekDayNumbersList)
                    {
                        // Does the section meet on this day?
                        if (section.meeting_days.Contains(item.Value.ToString()))
                        {
                            if (blockColor != null && blockColor.Length > 0)
                            {
                                foreach (string color in blockColor)
                                {
                                    // Is the section selected
                                    // Need to parse out the color in the section description
                                    //string sectionBlockColor = ClassSchedule.Instance.BlockColor(section.description);
                                    string sectionBlockColor = ClassSchedule.Instance.BlockColor(section.section_title);

                                    if (sectionBlockColor == color)
                                    {
                                        UserSectionDaySchedule userSectionDaySchedule = new UserSectionDaySchedule();
                                        userSectionDaySchedule.Day = item.Value;
                                        userSectionDaySchedule.Date = item.Key;
                                        userSectionDaySchedule.sectionId = section.id;

                                        userSectionDaySchedule.sectionEnrollmentId = enrollmentId;

                                        userSectionDayScheduleList.Add(userSectionDaySchedule);
                                    }
                                }
                            }
                        }
                    }

                    List<Attendance> attendanceList = new List<Attendance>();

                    foreach (UserSectionDaySchedule userSectionDaySchedule in userSectionDayScheduleList)
                    {

                        Attendance attendance = new Attendance();

                        // Set the attendance status and comment for each section
                        attendance.enrollment_id = userSectionDaySchedule.sectionEnrollmentId;
                        attendance.date = userSectionDaySchedule.Date.ToString("yyyy-MM-dd"); // Date
                        attendance.status = status;
                        attendance.comment = comment;

                        attendanceList.Add(attendance);

                    }
                    // Update the attendance using the Schoology API 
                    AttendanceApiJson attendanceApiJson = new AttendanceApiJson();
                    attendanceApiJson.attendances = new Attendances() { attendance = attendanceList };

                    if (attendanceApiJson.attendances.attendance.Count > 0)
                    {
                        string returnvalue = SectionService.Instance.PutSectionAttendance(section.id, attendanceApiJson);

                        // We need to wait some time in between API calls, otherwise we will get following error:
                        // WebException while reading response - The remote server returned an error: (429) Too Many Requests. - [_endPoint] - https://api.schoology.com/v1/sections/311928497
                        System.Threading.Thread.Sleep(250); // 500 = .5 seconds. If we still get the error, increase to 2 seconds or 2000
                    }

                }
            }

            // return the HTML success code or error

            return returnValue;

        }

        public List<int> WeekDayNumbers(DateTime startDate, DateTime endDate)
        {
            //DateTime startDate = new DateTime(2015, 6, 29);
            //DateTime endDate = new DateTime(2015, 7, 3);

            List<int> weekDaysNumbers = (from d in Enumerable.Range(0, (endDate - startDate).Days + 1)
                                         let currentDate1 = startDate.AddDays(d)
                                         where currentDate1.DayOfWeek != DayOfWeek.Saturday &&
                                         currentDate1.DayOfWeek != DayOfWeek.Sunday
                                         select (int)currentDate1.DayOfWeek).ToList();

            return weekDaysNumbers;
        }

        public List<DateTime> WeekDayDates(DateTime startDate, DateTime endDate)
        {
            List<DateTime> dates = (from d in Enumerable.Range(0, (endDate - startDate).Days + 1)
                                    let currentDate1 = startDate.AddDays(d)
                                    where currentDate1.DayOfWeek != DayOfWeek.Saturday &&
                                    currentDate1.DayOfWeek != DayOfWeek.Sunday
                                    select currentDate1).ToList();
            return dates;
        }

        public Dictionary<DateTime, int> WeekDayNumberDates(DateTime startDate, DateTime endDate)
        {
            List<int> keys = WeekDayNumbers(startDate, endDate);
            List<DateTime> values = WeekDayDates(startDate, endDate);

            var dict = Enumerable.Range(0, keys.Count).ToDictionary(i => values[i], i => keys[i]);

            return dict;
        }


    }
}
