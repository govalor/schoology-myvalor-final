﻿using System;
using System.IO;
using System.Net;
using System.Text;
using OAuth.Net.Common;
using OAuth.Net.Components;
using System.Collections.Specialized;
using log4net;


namespace Schoology.Api.Service
{
    public class ServiceProvider
    {
        // Log file
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const int RequestTimeOut = 1000 * 60 * 10;
        //private readonly string _consumerKey;
        //private readonly string _consumerSecret;

        private static string _endPoint;
        private static string _contentType;
        private static string _data;
        private static string _method;

        private Uri _serviceProviderUri;

        private static readonly ISigningProvider SigningProvider = new HmacSha1SigningProvider();
        private static readonly ISigningProvider SigningProviderPlainText = new PlaintextSigningProvider(false);
        private static readonly INonceProvider NonceProvider = new GuidNonceProvider();


        // Singleton Design Pattern
        private static readonly Lazy<ServiceProvider> lazy = new Lazy<ServiceProvider>(() => new ServiceProvider());
        public static ServiceProvider Instance
        { 
            get 
            { 
                return lazy.Value;
            } 
        }

        private ServiceProvider()
        {
        }

        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }

        public ServiceProvider(string endpoint, string consumerKey, string consumerSecret)
        {
            this.ConsumerKey = consumerKey;
            this.ConsumerSecret = consumerSecret;
            _serviceProviderUri = new Uri(endpoint);
        }

        private HttpWebRequest GenerateRequest(string contentType, string requestMethod)
        {
            var ts = UnixTime.ToUnixTime(DateTime.Now);
            //Create the needed OAuth Parameters. 
            //Refer - http://oauth.net/core/1.0/#sig_base_example
            var param = new OAuthParameters()
            {
                ConsumerKey = this.ConsumerKey,
                SignatureMethod = SigningProvider.SignatureMethod,
                //SignatureMethod = SigningProviderPlainText.SignatureMethod,
                Version = Constants.Version1_0,
                Nonce = NonceProvider.GenerateNonce(ts),
                Timestamp = ts.ToString(),
            };

            // Add the URL parameters to Oauth
            NameValueCollection collection = GetQueryParameters(_serviceProviderUri.Query);
            param.AdditionalParameters.Add(collection);

            //Generate Signature Hash
            var signatureBase = SignatureBase.Create(requestMethod.ToUpper(), _serviceProviderUri, param);
            //Set Signature Hash as one of the OAuth Parameter
            param.Signature = SigningProvider.ComputeSignature(signatureBase, this.ConsumerSecret, null);

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(_serviceProviderUri);
            httpWebRequest.Method = requestMethod;
            httpWebRequest.ContentType = contentType;
            httpWebRequest.Timeout = RequestTimeOut;
            //Add the OAuth Parameters to Authorization Header of Request
            httpWebRequest.Headers.Add(Constants.AuthorizationHeaderParameter, param.ToHeaderFormat());
            return httpWebRequest;
        }

        protected const string OAuthParameterPrefix = "oauth_";

        private NameValueCollection GetQueryParameters(string parameters)
        {
            if (parameters.StartsWith("?"))
            {
                parameters = parameters.Remove(0, 1);
            }

            NameValueCollection result = new NameValueCollection();

            if (!string.IsNullOrEmpty(parameters))
            {
                string[] p = parameters.Split('&');
                foreach (string s in p)
                {
                    if (!string.IsNullOrEmpty(s) && !s.StartsWith(OAuthParameterPrefix))
                    {
                        if (s.IndexOf('=') > -1)
                        {
                            string[] temp = s.Split('=');
                            result.Add(temp[0], temp[1]);
                        }
                        else
                        {
                            result.Add(s, string.Empty);
                        }
                    }
                }
            }

            return result;
        }

        public string GetData(string endPoint)
        {
            _endPoint = endPoint;

            _serviceProviderUri = new Uri(endPoint);

            _method = "GET";

            var request = GenerateRequest(string.Empty, WebRequestMethods.Http.Get);
            return GetRequestResponse(request);
        }

        public string DeleteData(string endPoint)
        {
            _endPoint = endPoint;

            _serviceProviderUri = new Uri(endPoint);

            _method = "DELETE";

            //log.InfoFormat("[ServiceProvider] - DeleteData -> API - [_endPoint] - {0}",  _endPoint);

            var request = GenerateRequest(string.Empty, "DELETE");
            return GetRequestResponse(request);
        }


        public string PostData(string endPoint, string contentType, string data)
        {
            _endPoint = endPoint;
            _contentType = contentType;
            _data = data;
            _method = "POST";

            _serviceProviderUri = new Uri(endPoint);

            var request = GenerateRequest(contentType, WebRequestMethods.Http.Post);
            var bytes = Encoding.ASCII.GetBytes(data);
            request.ContentLength = bytes.Length;
            if (bytes.Length > 0)
            {
                using (var requestStream = request.GetRequestStream())
                {
                    if (!requestStream.CanWrite) throw new Exception("The data cannot be written to request stream");
                    try
                    {
                        requestStream.Write(bytes, 0, bytes.Length);
                    }
                    catch (Exception exception)
                    {
                        throw new Exception(string.Format("Error while writing data to request stream - {0}", exception.Message));
                    }
                }
            }
            return GetRequestResponse(request);
        }

        public string PutData(string endPoint, string contentType, string data)
        {
            _endPoint = endPoint;
            _contentType = contentType;
            _data = data;
            _method = "PUT";

            _serviceProviderUri = new Uri(endPoint);

            var request = GenerateRequest(contentType, WebRequestMethods.Http.Put);
            var bytes = Encoding.ASCII.GetBytes(data);
            request.ContentLength = bytes.Length;
            if (bytes.Length > 0)
            {
                using (var requestStream = request.GetRequestStream())
                {
                    if (!requestStream.CanWrite) throw new Exception("The data cannot be written to request stream");
                    try
                    {
                        requestStream.Write(bytes, 0, bytes.Length);
                    }
                    catch (Exception exception)
                    {
                        throw new Exception(string.Format("Error while writing data to request stream - {0}", exception.Message));
                    }
                }
            }
            return GetRequestResponse(request);
        }

        // removed the static
        private string GetRequestResponse(HttpWebRequest httpWebRequest)
        {
            if (httpWebRequest == null) throw new ArgumentNullException("httpWebRequest");

            string responseString = null;

            try
            {
                using (var response = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                        {
                            var reader = new StreamReader(responseStream);
                            responseString = reader.ReadToEnd();
                            reader.Close();
                            responseStream.Close();
                        }
                    }
                    response.Close();
                }
            }
            catch (WebException webException)
            {
                //throw new Exception(string.Format("WebException while reading response - {0}", webException.Message));

                log.ErrorFormat("[GetRequestResponse] - WebException while reading response - {0} - [_method] - {1} - [_endPoint] - {2} - [_contentType] - {3} - [data] - {4} - [responseString] - {5}", webException.Message, _method, _endPoint, _contentType, _data, responseString);

            }
            catch (Exception exception)
            {
                //throw new Exception(string.Format("Unhandled exception while reading response - {0}", exception.Message));

                log.ErrorFormat("[GetRequestResponse] - Exception while reading response - {0} - [_method] - {1} - [_endPoint] - {2} - [_contentType] - {3} - [data] - {4} - [responseString] - {5}", exception.Message, _method, _endPoint, _contentType, _data, responseString);
            }

            _endPoint = "";
            _contentType = "";
            _data = "";
            _method = "";

            // We need to wait some time in between API calls, otherwise we will get following error:
            // WebException while reading response - The remote server returned an error: (429) Too Many Requests. - [_endPoint] - https://api.schoology.com/v1/sections/311928497
            System.Threading.Thread.Sleep(125); // 1000 = 1 second. If we still get the error, increase to 2 seconds or 2000

            return responseString;
        }

    }
}
